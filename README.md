# nigHTCap

## Notes and warnings:
* Work in progress: this tool is not yet fully functional
* **WARNING**: This software will wipe your device and void your warranty! Use at your own risk. 
* It has not been tested on a wide variety of HTC devices. If it doesn't work for your device you can use the manual method through htcdev.com, but please open an issue so it can be fixed! 
* Devices that were distributed by Verizon are not unlockable through this method. Other carriers might also have restrictions in place.

### Incompatible devices
Some devices require updates before they can be flashed, this is not handled in this software. Verify that your device is *not* one of the following:
* Droid Eris 
* EVO 3D (Sprint) 
* HTC A315c 
* HTC A6390 
* HTC Aria 
* HTC ChaCha 
* HTC Desire 
* HTC Desire HD 
* HTC Desire Z A7272 
* HTC Dream 
* HTC Droid Incredible 
* HTC Hero 
* HTC Legend 
* HTC Magic 
* HTC Merge 
* HTC Salsa 
* HTC Tattoo 
* HTC Tianxi A9188 
* HTC Wildfire 
* HTC Wildfire A3360 
* HTC Wildfire A3380 
* HTC Wildfire S 
* myTouch 3G Slide 
* myTouch 4G 
* T-Mobile G2 

## Installation instructions

### Debian based
```
sudo apt install python3-pip fastboot git
git clone https://gitlab.com/eloydegen/nightcap
cd htc-unlock
pip3 install -r requirements.txt
```

### Other operating systems 
TODO

## Device unlock procedure 


All new HTC devices shipped since One M9 support download mode instead of bootloader mode for this step. And for select HTC devices with Android Lollipop and all devices that receive Marshmallow, you will need to first manually enable OEM unlocking in Developer options in order to allow the bootloader to be unlocked and proceed with the next steps. To access the OEM unlocking setting on your device, open the Developer options in the system Settings. On Android 4.2 and higher, the Developer options screen is hidden by default. To make it visible, go to Settings > About > Software information > More and tap Build number seven times. Return to the Settings screen to find Developer options and check OEM unlocking.

### Steps
* Connect device using USB
* Enter fastboot mode by pressing VOL DOWN + POWER (sometimes it might help to remove the battery once and try again)
* `./htcunlock.py`

## Flags

`--own-mail`: use your own mailaddress instead of the default Guerillamail.

## How does it work? 
This software creates an account at HTCdev.com through a Guerillamail account and submits the Device Identifier Token to retrieve a binary file that is flashed to the phone over fastboot. This unlocks the bootloader and enables flashing custom software.

## Todo
* Document other methods: https://forum.xda-developers.com/showthread.php?t=1751796


## Security terminology  
S-ON/S-OFF: When the state is "S-ON" (Security-ON), the integrity of paritions is enabled. Unlocking the bootloader *only* disables integrity checking for the boot, system and recovery partitions, not partitions like radio which includes the modem firmware data. Changing the state to S-OFF is not always neccesary.

Official HTC docs: https://www.htcdev.com/bootloader/about_unlock_process

HBOOT is a SPL (secondary program loader) which can be overwritten when the state is S-OFF. The IPL (initial program loader) appears to be always read-only.

It is unknown wether a wrongly flashed HBOOT image can be fixed using the IPL.

## Interesting links
* HBOOT updater: https://github.com/wuzhenda/mirror_repo/blob/master/alpharev.sh

### Custom bootsplash 
This requires S-OFF. When the phone boots, it shows a splash screen embedded into the secondary program loader (HBOOT) binary before switching to the Linux kernel. When the security state is set to S-OFF, it's possible to replace this blob with a modded one that contains a custom splash screen by flashing a zip. In the beginning this was done trough recovery but the newer method uses RUU (ROM Update Utility) mode. For a working bootsplash generator, see: http://jobiwan.net:81/bootsplash-m8

See forum post: https://forum.xda-developers.com/showthread.php?p=39216236#post39216236

`ffmpeg -i splash1.bmp -f rawvideo -pix_fmt rgb565 splash.565`

Using a script this file is moved to `/cache` and then `dd`'ed to some /dev/mmc block device that contains the orginal bitmap. 

Credits to touch of jobo on XDA-Developers for the development.
