#!/usr/bin/python3.7
import sys
import re
from bs4 import BeautifulSoup
from requests_html import HTMLSession
from shutil import which
from subprocess import Popen, PIPE
import time
from guerrillamail import GuerrillaMailSession
import random
import string
from urllib.parse import urljoin
import json
from requests_toolbelt.utils import dump


def read_guerilla():
	session = GuerrillaMailSession()
	print(session.get_session_state()['email_address'])
	htc_mail = ""
	bool_print_once = False
	while True:
		try:
			if bool_print_once is False:
				print("Waiting for confirmation email to arrive...")
				bool_print_once = True
			# The first welcome mail is [0] in the list, the HTCdev.com is the second. If this mail is not received, the function returns an out of bounds. Loop until this works fine.
			email = session.get_email(session.get_email_list()[1].guid)
			print(email.body)
			htc_mail = email.body
			time.sleep(5)
			break
		except:
			continue
	# Go parse HTC mail
	
def get_all_forms(url):
    """Returns all form tags found on a web page's `url` """
    # GET request
    res = session.get(url)
    # for javascript driven website
    #res.html.render()
    soup = BeautifulSoup(res.html.html, "html.parser")
    return soup.find_all("form")


def get_form_details(form):
    """Returns the HTML details of a form,
    including action, method and list of form controls (inputs, etc)"""
    details = {}
    # get the form action (requested URL)
    action = form.attrs.get("action").lower()
    # get the form method (POST, GET, DELETE, etc)
    # if not specified, GET is the default in HTML
    method = form.attrs.get("method", "get").lower()
    # get all form inputs
    inputs = []
    for input_tag in form.find_all("input"):
        # get type of input form control
        input_type = input_tag.attrs.get("type", "text")
        # get name attribute
        input_name = input_tag.attrs.get("name")
        # get the default value of that input tag
        input_value =input_tag.attrs.get("value", "")
        # add everything to that list
        inputs.append({"type": input_type, "name": input_name, "value": input_value})
    # put everything to the resulting dictionary
    details["action"] = action
    details["method"] = method
    details["inputs"] = inputs
    return details

# get and parse identifier token

# parse form
# submit
# run guerilla()
# open link to activate
# submit identifier token
# get binary file

# fastboot flash unlocktoken Unlock_code.bin  

# todo idea: use flag for own mail address


# Using the Python implementation of fastboot would be nicer but I can't get the package to work
def fastboot_is_available():
	if which("fastboot") is None:
		print("Fastboot is not available, install it from your distro repositories.")
		quit()
	else:
		return True

def fastboot_get_identifier_token():
	# Fastboot prints output to stderr
	proc = Popen(["fastboot", "oem", "get_identifier_token"], stderr=PIPE)
	# Declare boolean to print the wait message only once while waiting for fastboot output
	first = True
	while proc.poll() is None:
		if first is True:
			print("Connect the phone in fastboot mode.")
			first = False
	# communicate() returns tuple, second value contains stderr
	# Pass list to parse function
	print("in fastboot get ide token")
	return fastboot_parse_identifier_token(proc.communicate()[1].decode("utf-8"))

def fastboot_parse_identifier_token(token):
	# TODO refactor to just string manipulation...
	# TODO handle other prefixes then (bootloader) such as INFO
	token = token.splitlines()

	# Remove the first elements until start of actual unlock code
	index_to_remove = 0
	for i, elem in enumerate(token):
		if '(bootloader) <<<< Identifier Token Start >>>>' in elem:
			print(i)
			index_to_remove = i
	token = token[index_to_remove:]

	# Remove the last elements after the unlock code
	for i, elem in enumerate(token):
		if '(bootloader) <<<<< Identifier Token End >>>>>' in elem:
			index_to_remove = i	

	token = token[:index_to_remove+1]

	# Remove the prefix on every line of the unlock code
	for i in range(len(token)):
		token[i] = token[i].strip("(bootloader) ")

	print("in parse")
	return '\r\n'.join(token)

def gen_random_string():
	return ''.join(random.choices(string.ascii_lowercase, k=10))

def gen_random_password():
	return ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=10))

def parse_pages():
	for link in soup.find_all("link"):
		try:
			link.attrs["href"] = urljoin(url, link.attrs["href"])
		except:
			pass
	for script in soup.find_all("script"):
		try:
			script.attrs["src"] = urljoin(url, script.attrs["src"])
		except:
			pass
	for img in soup.find_all("img"):
		try:
			img.attrs["src"] = urljoin(url, img.attrs["src"])
		except:
			pass
	for a in soup.find_all("a"):
		try:
			a.attrs["href"] = urljoin(url, a.attrs["href"])
		except:
			pass

def print_form_details(forms):
	for i, form in enumerate(forms, start=1):
	    form_details = get_form_details(form)
	    print("="*50, f"form #{i}", "="*50)
	    print(form_details)

if __name__ == '__main__':
	session = HTMLSession()

	token = fastboot_get_identifier_token()
	print(token)

	# Get the form
	url = "https://www.htcdev.com/"

	forms = get_all_forms(url)
	
	sign_in_form = get_all_forms(url)[0]
	sign_in_form_details = get_form_details(sign_in_form)

	# the data body we want to submit
	data = {}
	for input_tag in sign_in_form_details["inputs"]:
		if input_tag["type"] == "hidden":
		# if it's hidden, use the default value
			data[input_tag["name"]] = input_tag["value"]
		elif input_tag["type"] != "submit":
                    None

	# Yeah this is public, please don't change them! :-)
	data["username"] = "imnotsure" 
	data["password"] = "dr7qb7LtB7N9uKW"

	# join the url with the action (form request URL)
	url = urljoin(url, sign_in_form_details["action"])
	
	# Login
	res = session.post(url, data=data)

	# the below code is only for replacing relative URLs to absolute ones
	soup = BeautifulSoup(res.content, "html.parser")
	
	# Login is done, start with submitting the token
	
	unlock_url = "https://www.htcdev.com/bootloader/unlock-instructions/page-2"
	res = session.get(unlock_url)
	forms = get_all_forms(unlock_url)
	
	unlock_form = get_all_forms(unlock_url)[0]
	unlock_form_details = get_form_details(unlock_form)
	print(unlock_form_details)
	print("unlock form details above")

	# join the url with the action (form request URL)
	unlock_url = urljoin(unlock_url, unlock_form_details["action"])
	# the data body we want to submit
	alldet = {}
	#for input_tag in sign_in_form_details["inputs"]:
	#	if input_tag["type"] == "hidden":
	#	# if it's hidden, use the default value
	#		form_details[input_tag["name"]] = input_tag["value"]
	#	elif input_tag["type"] != "submit":
        #            None
	#token = token.replace(" ", "+")
	print("token:")
	print(token)
	append = "asdf&bootloader_text=" + token
	alldet = {'meme': append}
	alldet["value"] = append 
	unlock_form_details["inputs"].append(alldet)
	print(unlock_form_details)
	alldet = unlock_form_details

	print(alldet)
	if sign_in_form_details["method"] == "post":
		res = session.post(unlock_url, data = {'bootloader_text': token})# data=alldet)
		print("res:")
		print(res)
	print("ja dump hier token attemtp:")
	print(dump.dump_all(res).decode("utf-8"))

	# Not yet working
	quit()
	result = re.search('URL=(.*)" http-equiv', str(soup)).group(1)
	fullurl = "https://htcdev.com" + result
	# check if contains Invalid string
	print(fullurl)
	if "Invalid" in fullurl: 
		quit()
	res = session.get(fullurl)
	print("oke nu meuk gaa ndumupen:")
	print(dump.dump_all(res).decode("utf-8"))
	soup = BeautifulSoup(res.content, "html.parser")
	for link in soup.find_all("link"):
		try:
			link.attrs["href"] = urljoin(unlock_url, link.attrs["href"])
		except:
			pass
	for script in soup.find_all("script"):
		try:
			script.attrs["src"] = urljoin(unlock_url, script.attrs["src"])
		except:
			pass
	for img in soup.find_all("img"):
		try:
			img.attrs["src"] = urljoin(unlock_url, img.attrs["src"])
		except:
			pass
	for a in soup.find_all("a"):
		try:
			a.attrs["href"] = urljoin(unlock_url, a.attrs["href"])
		except:
			pass
	open("unlock_result_after_redirect.html", "w").write(str(soup))
